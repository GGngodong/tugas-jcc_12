<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body style="font-family: sans-serif;">
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="welcome" method="post">
        @csrf
        <div>
            <label>First Name :</label>
            <div>
                <input type="text" name="first_n" required>
            </div>
        </div>
        <br>
        <div>
            <label>Last Name :</label>
            <div>
                <input type="text" name="last_n" required>
            </div>
        </div>
        <br>
        <div>
            <label>Gender</label>
            <div>
                <input type="radio" name="Gender" value="0">Male <br>
                <input type="radio" name="Gender" value="1">Female <br>
            </div>
        </div>
        <br>
        <div>
            <label>Nationality</label>
            <div>
                <select id="Nationality" required>
                <option value="Indonesia">Indonesia</option>
                <option value="westgerman">West German</option>
                <option value="Russia">Russia</option>
                <option value="Soviet">Soviet Union</option>
                <option value="Zimbabwe">Zimbabwe</option>
                <option value="Vormir">Vormir</option>
                <option value="sakar">Saakar</option>
                <option value="asgard">Asgard</option>
                <option value="tva">The End Of time</option>
                </select>
            </div>
        </div>
        <br>
        <div>
            <label>Language Spoken</label>
            <div>
                <input type="checkbox" id="id" name="lang" value="id">
                <label for="id">Bahasa Indonesia</label> <br>
                <input type="checkbox" id="en" name="lang" value="en">
                <label for="en">English</label> <br>
                <input type="checkbox" id="ot" name="lang" value="oth">
                <label for="ot">Other</label> <br>
                
            </div>
        </div>
        <br>
        <div>
            <label>Bio</label>
            <div>
                <textarea cols="50" rows="7" id="pesanuser" required>Pesan saya adalah...</textarea><br />
            </div>
        </div>
        <div>
            <input type="submit" name="signup" value="Sign Up">
        </div>
    </form>
</body>
</html>