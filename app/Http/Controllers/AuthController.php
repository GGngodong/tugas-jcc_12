<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('register');
    }
    
    public function welcome(Request $request)
    {
        // dd($request->all());
        $name_f = $request['first_n'];
        $name_l = $request['last_n'];
        return view('welcome', compact('name_f', 'name_l'));
    }
}
